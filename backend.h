#include <EGL/egl.h>
#include <X11/Xlib.h>

struct modifier_state {
	uint32_t depressed, latched, locked, group;
};

static struct BackWindow {
	Window window;
	EGLSurface surface;
} window;

struct callbacks {
	void (*resize) (Window w, int width, int height);
	void (*draw) (Window w);
	void (*mouse_motion) (Window w, int x, int y);
	void (*mouse_button) (Window w, int button, int state);
	void (*key) (Window w, int key, int state);
	void (*modifiers) (struct modifier_state modifier_state);
	void (*destroy) (Window w);
	void (*focus_in)(Window w);
	void (*focus_out)(Window w);
};

void backend_init (struct callbacks *callbacks);
EGLDisplay backend_get_egl_display (void);
void backend_make_current(struct BackWindow window);
void backend_swap_buffers (struct BackWindow window);
void backend_dispatch_nonblocking (void);
void backend_wait_for_events (int wayland_fd);
void backend_get_keymap (int *fd, int *size);
long backend_get_timestamp (void);
struct BackWindow backend_create_window (int width, int height);
void backend_destroy_window(struct BackWindow window);
void backend_get_absolute_pointer(Window window, int* x, int* y);
void backend_set_window_position(struct BackWindow window, int x, int y);
void backend_get_window_position(struct BackWindow window, int* x, int* y);
void backend_resize_window(struct BackWindow window, int h, int w);
void backend_get_window_size(struct BackWindow window, int* h, int* w);
