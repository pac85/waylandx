#define _GNU_SOURCE
#include "backend.h"
#include <wayland-server.h>
#include <X11/Xutil.h>
#include <linux/input.h>
#include <EGL/egl.h>
#include <X11/Xlib-xcb.h>
#include <xkbcommon/xkbcommon-x11.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <poll.h>

#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080

EGLContext egl_context;
EGLConfig config;
static Display *x_display;
static EGLDisplay egl_display;
static struct callbacks callbacks;
static xcb_connection_t *xcb_connection;
static int32_t keyboard_device_id;
static struct xkb_keymap *keymap;
static struct xkb_state *state;

static void setup_egl() {
	// setup EGL
	EGLint attribs[] = {
		EGL_RED_SIZE, 1,
		EGL_GREEN_SIZE, 1,
		EGL_BLUE_SIZE, 1,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_BIT,
	EGL_NONE};
	EGLint num_configs_returned;
	eglChooseConfig (egl_display, attribs, &config, 1, &num_configs_returned);

	eglBindAPI (EGL_OPENGL_API);
	egl_context = eglCreateContext (egl_display, config, EGL_NO_CONTEXT, NULL);
}

struct BackWindow backend_create_window (int width, int height) {

    struct BackWindow window;
	
	// get the visual from the EGL config
	EGLint visual_id;
	eglGetConfigAttrib (egl_display, config, EGL_NATIVE_VISUAL_ID, &visual_id);
	XVisualInfo visual_template;
	visual_template.visualid = visual_id;
	visual_template.depth = 32;
	int num_visuals_returned;
	XVisualInfo *visual = XGetVisualInfo (x_display, VisualIDMask, &visual_template, &num_visuals_returned);
	
	// create a window
	XSetWindowAttributes window_attributes;
	window_attributes.event_mask = ExposureMask | StructureNotifyMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask | EnterWindowMask | LeaveWindowMask | FocusChangeMask;
	window_attributes.colormap = XCreateColormap (x_display, RootWindow(x_display,DefaultScreen(x_display)), visual->visual, AllocNone);
	window.window = XCreateWindow (
		x_display,
		RootWindow(x_display, DefaultScreen(x_display)),
		0, 0,
		width, height,
		0, // border width
		visual->depth, // depth
		InputOutput, // class
		visual->visual, // visual
		CWEventMask|CWColormap, // attribute mask
		&window_attributes // attributes
	);
	
	// EGL context and surface
	eglBindAPI (EGL_OPENGL_API);
	window.surface = eglCreateWindowSurface (egl_display, config, window.window, NULL);
	
	XFree (visual);
	
	XMapWindow (x_display, window.window);

    return window;
}

void backend_destroy_window(struct BackWindow window) {
	eglDestroySurface(egl_display, window.surface);
	XDestroyWindow(x_display, window.window);
}

void backend_get_absolute_pointer(Window window, int* x, int* y) {
	int dummy;
	Window dummyw;
	XQueryPointer(x_display, window, &dummyw, &dummyw, x, y, &dummy, &dummy, (unsigned int *)&dummy);
}

void backend_set_window_position(struct BackWindow window, int x, int y) {
    XMoveWindow(x_display, window.window, x, y);
}

void backend_get_window_position(struct BackWindow window, int* x, int* y) {
	XWindowAttributes attribs;
	XGetWindowAttributes(x_display, window.window, &attribs);
	*x = attribs.x;
	*y = attribs.y;
}

void backend_resize_window(struct BackWindow window, int h, int w) {
    XResizeWindow(x_display, window.window, w, h);
}

void backend_get_window_size(struct BackWindow window, int* h, int* w) {
	XWindowAttributes attribs;
	XGetWindowAttributes(x_display, window.window, &attribs);
	*w = attribs.width;
	*h = attribs.height;
}

void backend_init (struct callbacks *_callbacks) {
	callbacks = *_callbacks;
	x_display = XOpenDisplay (NULL);
	
	xcb_connection = XGetXCBConnection (x_display);
	struct xkb_context *context = xkb_context_new (XKB_CONTEXT_NO_FLAGS);
	xkb_x11_setup_xkb_extension (xcb_connection, XKB_X11_MIN_MAJOR_XKB_VERSION, XKB_X11_MIN_MINOR_XKB_VERSION, 0, NULL, NULL, NULL, NULL);
	keyboard_device_id = xkb_x11_get_core_keyboard_device_id (xcb_connection);
	keymap = xkb_x11_keymap_new_from_device (context, xcb_connection, keyboard_device_id, XKB_KEYMAP_COMPILE_NO_FLAGS);
	state = xkb_x11_state_new_from_device (keymap, xcb_connection, keyboard_device_id);
	
	egl_display = eglGetDisplay (x_display);
	eglInitialize (egl_display, NULL, NULL);
    setup_egl();
}

EGLDisplay backend_get_egl_display (void) {
	return egl_display;
}

void backend_make_current(struct BackWindow window) {
	eglMakeCurrent (egl_display, window.surface, window.surface, egl_context);
}

void backend_swap_buffers (struct BackWindow window) {
	eglSwapBuffers (egl_display, window.surface);
}

static void update_modifiers (void) {
	struct modifier_state modifier_state;
	modifier_state.depressed = xkb_state_serialize_mods (state, XKB_STATE_MODS_DEPRESSED);
	modifier_state.latched = xkb_state_serialize_mods (state, XKB_STATE_MODS_LATCHED);
	modifier_state.locked = xkb_state_serialize_mods (state, XKB_STATE_MODS_LOCKED);
	modifier_state.group = xkb_state_serialize_layout (state, XKB_STATE_LAYOUT_EFFECTIVE);
	callbacks.modifiers (modifier_state);
}

void backend_dispatch_nonblocking (void) {
	XEvent event;
	while (XPending(x_display)) {
		XNextEvent (x_display, &event);
		if (event.type == ConfigureNotify) {
			callbacks.resize (event.xconfigure.window, event.xconfigure.width, event.xconfigure.height);
		}
		else if (event.type == Expose) {
			callbacks.draw (event.xexpose.window);
		}
		else if (event.type == MotionNotify) {
			callbacks.mouse_motion (event.xbutton.window, event.xbutton.x, event.xbutton.y);
		}
		else if (event.type == ButtonPress) {
			if (event.xbutton.button == Button1)
				callbacks.mouse_button (event.xbutton.window, BTN_LEFT, WL_POINTER_BUTTON_STATE_PRESSED);
			else if (event.xbutton.button == Button2)
				callbacks.mouse_button (event.xbutton.window, BTN_MIDDLE, WL_POINTER_BUTTON_STATE_PRESSED);
			else if (event.xbutton.button == Button3)
				callbacks.mouse_button (event.xbutton.window, BTN_RIGHT, WL_POINTER_BUTTON_STATE_PRESSED);
		}
		else if (event.type == ButtonRelease) {
			if (event.xbutton.button == Button1)
				callbacks.mouse_button (event.xbutton.window, BTN_LEFT, WL_POINTER_BUTTON_STATE_RELEASED);
			else if (event.xbutton.button == Button2)
				callbacks.mouse_button (event.xbutton.window, BTN_MIDDLE, WL_POINTER_BUTTON_STATE_RELEASED);
			else if (event.xbutton.button == Button3)
				callbacks.mouse_button (event.xbutton.window, BTN_RIGHT, WL_POINTER_BUTTON_STATE_RELEASED);
		}
		else if (event.type == KeyPress) {
			callbacks.key (event.xkey.window, event.xkey.keycode - 8, WL_KEYBOARD_KEY_STATE_PRESSED);
			xkb_state_update_key (state, event.xkey.keycode, XKB_KEY_DOWN);
			update_modifiers ();
		}
		else if (event.type == KeyRelease) {
			callbacks.key (event.xkey.window, event.xkey.keycode - 8, WL_KEYBOARD_KEY_STATE_RELEASED);
			xkb_state_update_key (state, event.xkey.keycode, XKB_KEY_UP);
			update_modifiers ();
		}
		else if (event.type == FocusIn) {
			xkb_state_unref (state);
			state = xkb_x11_state_new_from_device (keymap, xcb_connection, keyboard_device_id);
			update_modifiers ();
		}
		else if (event.type == DestroyNotify) {
			callbacks.destroy(event.xdestroywindow.window);
		}
	}
}

void backend_wait_for_events (int wayland_fd) {
	struct pollfd fds[2] = {{ConnectionNumber(x_display), POLLIN}, {wayland_fd, POLLIN}};
	poll (fds, 2, -1);
}

void backend_get_keymap (int *fd, int *size) {
	char *string = xkb_keymap_get_as_string (keymap, XKB_KEYMAP_FORMAT_TEXT_V1);
	*size = strlen (string) + 1;
	char *xdg_runtime_dir = getenv ("XDG_RUNTIME_DIR");
	*fd = open (xdg_runtime_dir, O_TMPFILE|O_RDWR|O_EXCL, 0600);
	ftruncate (*fd, *size);
	char *map = mmap (NULL, *size, PROT_READ|PROT_WRITE, MAP_SHARED, *fd, 0);
	strcpy (map, string);
	munmap (map, *size);
	free (string);
}

long backend_get_timestamp (void) {
	struct timespec t;
	clock_gettime (CLOCK_MONOTONIC, &t);
	return t.tv_sec * 1000 + t.tv_nsec / 1000000;
}
